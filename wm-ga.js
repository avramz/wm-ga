(function(wmAnalytics, undefined) {
    var supports = !!document.querySelector && !!window.addEventListener,
        elements = [],
        _wmGA,
        TRIGGER_EVENTS_MAP = {
            "click": "click",
            "hover": "mouseenter",
            "inView": "inView"
        };

    wmAnalytics.init = function (tracker) {
        if (!supports) {
            throw ("Your browser doesn't support this plugin.");
        }

        _wmGA = tracker;

        this.trackPageView();

        elements = document.querySelectorAll('[data-wmga-on]');

        for (var i = 0, max = elements.length; i < max; i++) {
            _attachEvents(elements[i]);
        }
    };

    /**
     * Attach event listeners
     * @param element
     * @private
     */
    function _attachEvents(element) {
        var trigger = element.getAttribute('data-wmga-on'),
            event = element.getAttribute('data-wmga-event'),
            category = element.getAttribute('data-wmga-category');

        if (!TRIGGER_EVENTS_MAP[trigger]) {
            throw ('Unsupported event' + trigger);
        }

        try {
            if (element.addEventListener) {
                element.addEventListener(TRIGGER_EVENTS_MAP[trigger], _evtCabllback, false);
            } else if (element.attachEvent) {
                element.attachEvent('on' + TRIGGER_EVENTS_MAP[trigger], _evtCabllback);
            }
        }
        catch (e) {
            console.log(e);
        }

        function _evtCabllback() {
            _track(trigger, event, category)
        }
    }

    /**
     * Attempt to track event
     * @param trigger
     * @param event
     * @param category
     * @private
     */
    function _track(trigger, event, category) {
        _wmGA.send({
            hitType: 'event',
            eventCategory: category,
            eventAction: trigger,
            eventLabel: event
        });
    }

    wmAnalytics.trackEvent = function (options) {
        _track(options['action'], options['label'], options['category']);
    };

    wmAnalytics.trackPageView = function () {
        _wmGA.send('pageview');
    };

    return wmAnalytics;

})(window.wmAnalytics = window.wmAnalytics || {});